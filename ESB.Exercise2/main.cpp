//Playing Cards
//Eric Brown
#include <iostream>
#include <conio.h>
#include <string>

using namespace std;



enum Rank
{
	Two = 2,Three,Four,Five,Six,Seven,Eight,Nine,Ten,Jack,Queen,King,Ace
};

enum Suit
{
	Clubs,Spades,Diamonds,Hearts
};

struct Card
{
	Rank Rank;
	Suit Suit;
};

void PrintCard(Card card);
Card HighCard(Card card1, Card card2);
int main()
{
	Card c1;
	Card c2;

	c1.Rank = Ace;
	c1.Suit = Hearts;
	c2.Rank = Four;
	c2.Suit = Diamonds;

	PrintCard(HighCard(c1, c2));
	
	


	
	_getch();
	return 0;
}

void PrintCard(Card card)
{
	switch (card.Rank)
	{
	case Two: cout << "the two of "; break;
	case Three: cout << "the Three of "; break;
	case Four: cout << "the Four of "; break;
	case Five: cout << "the Five of "; break;
	case Six: cout << "the Six of "; break;
	case Seven: cout << "the Seven of "; break;
	case Eight: cout << "the Eight of "; break;
	case Nine: cout << "the Nine of "; break;
	case Ten: cout << "the Ten of "; break;
	case Jack: cout << "the Jack of "; break;
	case Queen: cout << "the Queen of "; break;
	case King: cout << "the King of "; break;
	default: cout << "the Ace of "; break;
		break;
	}
	switch (card.Suit)
	{
	case Hearts: cout << " Hearts"; break;
	case Diamonds: cout << " Diamonds "; break;
	case Clubs: cout << " Clubs "; break;
	
	default:  cout << " Spades "; break;
		break;
	}
}

Card HighCard(Card card1, Card card2)
{
	if (card1.Rank > card2.Rank) {
		
		return card1;
	}
	else {
		
		return card2;
	}
}